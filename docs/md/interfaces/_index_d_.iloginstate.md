[@ticmakers-react-native/login](../README.md) > ["index.d"](../modules/_index_d_.md) > [ILoginState](../interfaces/_index_d_.iloginstate.md)

# Interface: ILoginState

Interface to define the state of the Login component

*__interface__*: ILoginState

## Hierarchy

**ILoginState**

## Index

---

