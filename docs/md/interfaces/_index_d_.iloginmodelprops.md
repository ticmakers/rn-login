[@ticmakers-react-native/login](../README.md) > ["index.d"](../modules/_index_d_.md) > [ILoginModelProps](../interfaces/_index_d_.iloginmodelprops.md)

# Interface: ILoginModelProps

Interface to define the props of the username and password

*__interface__*: ILoginModelProps

## Hierarchy

**ILoginModelProps**

## Index

### Properties

* [icon](_index_d_.iloginmodelprops.md#icon)
* [label](_index_d_.iloginmodelprops.md#label)
* [rules](_index_d_.iloginmodelprops.md#rules)
* [value](_index_d_.iloginmodelprops.md#value)

---

## Properties

<a id="icon"></a>

### `<Optional>` icon

**● icon**: *`IIconProps` \| `TypeComponent`*

*Defined in index.d.ts:51*

Set a IIconProps or a React-Native component to show a icon for the input

*__type__*: {(IIconProps \| TypeComponent)}

___
<a id="label"></a>

### `<Optional>` label

**● label**: *`undefined` \| `string`*

*Defined in index.d.ts:39*

Label to show in the input

*__type__*: {string}

___
<a id="rules"></a>

### `<Optional>` rules

**● rules**: *`IInputRules`*

*Defined in index.d.ts:45*

Define the rules validation for the input

*__type__*: {IInputRules}

___
<a id="value"></a>

### `<Optional>` value

**● value**: *`any`*

*Defined in index.d.ts:57*

Set a default value to the input

*__type__*: {\*}

___

