[@ticmakers-react-native/login](../README.md) > ["index.d"](../modules/_index_d_.md) > [Login](../classes/_index_d_.login.md)

# Class: Login

Class to define the Login component

*__class__*: Login

*__extends__*: {React.Component<ILoginProps, ILoginState>}

## Type parameters
#### SS 
## Hierarchy

 `Component`<[ILoginProps](../interfaces/_index_d_.iloginprops.md), [ILoginState](../interfaces/_index_d_.iloginstate.md)>

**↳ Login**

## Index

### Constructors

* [constructor](_index_d_.login.md#constructor)

### Properties

* [context](_index_d_.login.md#context)
* [labels](_index_d_.login.md#labels)
* [passInput](_index_d_.login.md#passinput)
* [props](_index_d_.login.md#props)
* [refs](_index_d_.login.md#refs)
* [state](_index_d_.login.md#state)
* [userInput](_index_d_.login.md#userinput)
* [contextType](_index_d_.login.md#contexttype)

### Methods

* [FacebookButton](_index_d_.login.md#facebookbutton)
* [Footer](_index_d_.login.md#footer)
* [Form](_index_d_.login.md#form)
* [GoogleButton](_index_d_.login.md#googlebutton)
* [Logo](_index_d_.login.md#logo)
* [SubmitButton](_index_d_.login.md#submitbutton)
* [UNSAFE_componentWillMount](_index_d_.login.md#unsafe_componentwillmount)
* [UNSAFE_componentWillReceiveProps](_index_d_.login.md#unsafe_componentwillreceiveprops)
* [UNSAFE_componentWillUpdate](_index_d_.login.md#unsafe_componentwillupdate)
* [_isLight](_index_d_.login.md#_islight)
* [_onSubmit](_index_d_.login.md#_onsubmit)
* [_onSubmitFacebook](_index_d_.login.md#_onsubmitfacebook)
* [_onSubmitGoogle](_index_d_.login.md#_onsubmitgoogle)
* [_processProps](_index_d_.login.md#_processprops)
* [componentDidCatch](_index_d_.login.md#componentdidcatch)
* [componentDidMount](_index_d_.login.md#componentdidmount)
* [componentDidUpdate](_index_d_.login.md#componentdidupdate)
* [componentWillMount](_index_d_.login.md#componentwillmount)
* [componentWillReceiveProps](_index_d_.login.md#componentwillreceiveprops)
* [componentWillUnmount](_index_d_.login.md#componentwillunmount)
* [componentWillUpdate](_index_d_.login.md#componentwillupdate)
* [forceUpdate](_index_d_.login.md#forceupdate)
* [getSnapshotBeforeUpdate](_index_d_.login.md#getsnapshotbeforeupdate)
* [isValid](_index_d_.login.md#isvalid)
* [render](_index_d_.login.md#render)
* [setState](_index_d_.login.md#setstate)
* [shouldComponentUpdate](_index_d_.login.md#shouldcomponentupdate)

---

## Constructors

<a id="constructor"></a>

###  constructor

⊕ **new Login**(props: *`Readonly`<[ILoginProps](../interfaces/_index_d_.iloginprops.md)>*): [Login](_index_d_.login.md)

⊕ **new Login**(props: *[ILoginProps](../interfaces/_index_d_.iloginprops.md)*, context?: *`any`*): [Login](_index_d_.login.md)

*Inherited from Component.__constructor*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:425*

**Parameters:**

| Name | Type |
| ------ | ------ |
| props | `Readonly`<[ILoginProps](../interfaces/_index_d_.iloginprops.md)> |

**Returns:** [Login](_index_d_.login.md)

*Inherited from Component.__constructor*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:427*

*__deprecated__*: 

*__see__*: [https://reactjs.org/docs/legacy-context.html](https://reactjs.org/docs/legacy-context.html)

**Parameters:**

| Name | Type |
| ------ | ------ |
| props | [ILoginProps](../interfaces/_index_d_.iloginprops.md) |
| `Optional` context | `any` |

**Returns:** [Login](_index_d_.login.md)

___

## Properties

<a id="context"></a>

###  context

**● context**: *`any`*

*Inherited from Component.context*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:425*

If using the new style context, re-declare this in your class to be the `React.ContextType` of your `static contextType`.

```ts
static contextType = MyContext
context!: React.ContextType<typeof MyContext>
```

*__deprecated__*: if used without a type annotation, or without static contextType

*__see__*: [https://reactjs.org/docs/legacy-context.html](https://reactjs.org/docs/legacy-context.html)

___
<a id="labels"></a>

###  labels

**● labels**: *`object`*

*Defined in index.d.ts:363*

Default locale labels

#### Type declaration

 en: `object`

 password: `string`

 username: `string`

 es: `object`

 password: `string`

 username: `string`

___
<a id="passinput"></a>

###  passInput

**● passInput**: *`Input` \| `undefined`*

*Defined in index.d.ts:358*

Reference of password input

*__type__*: {(Input \| undefined)}

___
<a id="props"></a>

###  props

**● props**: *`Readonly`<[ILoginProps](../interfaces/_index_d_.iloginprops.md)> & `Readonly`<`object`>*

*Inherited from Component.props*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:450*

___
<a id="refs"></a>

###  refs

**● refs**: *`object`*

*Inherited from Component.refs*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:456*

*__deprecated__*: [https://reactjs.org/docs/refs-and-the-dom.html#legacy-api-string-refs](https://reactjs.org/docs/refs-and-the-dom.html#legacy-api-string-refs)

#### Type declaration

[key: `string`]: `ReactInstance`

___
<a id="state"></a>

###  state

**● state**: *`Readonly`<[ILoginState](../interfaces/_index_d_.iloginstate.md)>*

*Inherited from Component.state*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:451*

___
<a id="userinput"></a>

###  userInput

**● userInput**: *`Input` \| `undefined`*

*Defined in index.d.ts:352*

Reference of username input

*__type__*: {(Input \| undefined)}

___
<a id="contexttype"></a>

### `<Static>``<Optional>` contextType

**● contextType**: *`Context`<`any`>*

*Inherited from Component.contextType*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:410*

If set, `this.context` will be set at runtime to the current value of the given Context.

Usage:

```ts
type MyContext = number
const Ctx = React.createContext<MyContext>(0)

class Foo extends React.Component {
  static contextType = Ctx
  context!: React.ContextType<typeof Ctx>
  render () {
    return <>My context's value: {this.context}</>;
  }
}
```

*__see__*: [https://reactjs.org/docs/context.html#classcontexttype](https://reactjs.org/docs/context.html#classcontexttype)

___

## Methods

<a id="facebookbutton"></a>

###  FacebookButton

▸ **FacebookButton**(): `TypeComponent`

*Defined in index.d.ts:402*

Method that renders the Facebook button component

**Returns:** `TypeComponent`

___
<a id="footer"></a>

###  Footer

▸ **Footer**(): `TypeComponent`

*Defined in index.d.ts:414*

Method that renders the Footer component

**Returns:** `TypeComponent`

___
<a id="form"></a>

###  Form

▸ **Form**(): `TypeComponent`

*Defined in index.d.ts:390*

Method that renders the Form component

**Returns:** `TypeComponent`

___
<a id="googlebutton"></a>

###  GoogleButton

▸ **GoogleButton**(): `TypeComponent`

*Defined in index.d.ts:408*

Method that renders the Google button component

**Returns:** `TypeComponent`

___
<a id="logo"></a>

###  Logo

▸ **Logo**(): `TypeComponent`

*Defined in index.d.ts:384*

Method that renders the Logo component

**Returns:** `TypeComponent`

___
<a id="submitbutton"></a>

###  SubmitButton

▸ **SubmitButton**(): `TypeComponent`

*Defined in index.d.ts:396*

Method that renders the Submit button component

**Returns:** `TypeComponent`

___
<a id="unsafe_componentwillmount"></a>

### `<Optional>` UNSAFE_componentWillMount

▸ **UNSAFE_componentWillMount**(): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:638*

Called immediately before mounting occurs, and before `Component#render`. Avoid introducing any side-effects or subscriptions in this method.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use componentDidMount or the constructor instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Returns:** `void`

___
<a id="unsafe_componentwillreceiveprops"></a>

### `<Optional>` UNSAFE_componentWillReceiveProps

▸ **UNSAFE_componentWillReceiveProps**(nextProps: *`Readonly`<[ILoginProps](../interfaces/_index_d_.iloginprops.md)>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillReceiveProps*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:670*

Called when the component may be receiving new props. React may call this even if props have not changed, so be sure to compare new and existing props if you only want to handle changes.

Calling `Component#setState` generally does not trigger this method.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use static getDerivedStateFromProps instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[ILoginProps](../interfaces/_index_d_.iloginprops.md)> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="unsafe_componentwillupdate"></a>

### `<Optional>` UNSAFE_componentWillUpdate

▸ **UNSAFE_componentWillUpdate**(nextProps: *`Readonly`<[ILoginProps](../interfaces/_index_d_.iloginprops.md)>*, nextState: *`Readonly`<[ILoginState](../interfaces/_index_d_.iloginstate.md)>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.UNSAFE_componentWillUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:698*

Called immediately before rendering when new props or state is received. Not called for the initial render.

Note: You cannot call `Component#setState` here.

This method will not stop working in React 17.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use getSnapshotBeforeUpdate instead

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[ILoginProps](../interfaces/_index_d_.iloginprops.md)> |
| nextState | `Readonly`<[ILoginState](../interfaces/_index_d_.iloginstate.md)> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="_islight"></a>

### `<Private>` _isLight

▸ **_isLight**(backgroundColor: *`string`*): `boolean`

*Defined in index.d.ts:449*

Method to valid if a background color is light

**Parameters:**

| Name | Type | Description |
| ------ | ------ | ------ |
| backgroundColor | `string` |  A string color rgb,rgba,hex,etc... |

**Returns:** `boolean`

___
<a id="_onsubmit"></a>

### `<Private>` _onSubmit

▸ **_onSubmit**(): `void`

*Defined in index.d.ts:427*

Method that fire when the submit button is pressed

**Returns:** `void`

___
<a id="_onsubmitfacebook"></a>

### `<Private>` _onSubmitFacebook

▸ **_onSubmitFacebook**(): `void`

*Defined in index.d.ts:434*

Method that fire when the button facebook is pressed

**Returns:** `void`

___
<a id="_onsubmitgoogle"></a>

### `<Private>` _onSubmitGoogle

▸ **_onSubmitGoogle**(): `void`

*Defined in index.d.ts:441*

Method that fire when the button google is pressed

**Returns:** `void`

___
<a id="_processprops"></a>

### `<Private>` _processProps

▸ **_processProps**(): [ILoginState](../interfaces/_index_d_.iloginstate.md)

*Defined in index.d.ts:456*

Method to process the props

**Returns:** [ILoginState](../interfaces/_index_d_.iloginstate.md)

___
<a id="componentdidcatch"></a>

### `<Optional>` componentDidCatch

▸ **componentDidCatch**(error: *`Error`*, errorInfo: *`ErrorInfo`*): `void`

*Inherited from ComponentLifecycle.componentDidCatch*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:567*

Catches exceptions generated in descendant components. Unhandled exceptions will cause the entire component tree to unmount.

**Parameters:**

| Name | Type |
| ------ | ------ |
| error | `Error` |
| errorInfo | `ErrorInfo` |

**Returns:** `void`

___
<a id="componentdidmount"></a>

### `<Optional>` componentDidMount

▸ **componentDidMount**(): `void`

*Inherited from ComponentLifecycle.componentDidMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:546*

Called immediately after a component is mounted. Setting state here will trigger re-rendering.

**Returns:** `void`

___
<a id="componentdidupdate"></a>

### `<Optional>` componentDidUpdate

▸ **componentDidUpdate**(prevProps: *`Readonly`<[ILoginProps](../interfaces/_index_d_.iloginprops.md)>*, prevState: *`Readonly`<[ILoginState](../interfaces/_index_d_.iloginstate.md)>*, snapshot?: *[SS]()*): `void`

*Inherited from NewLifecycle.componentDidUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:609*

Called immediately after updating occurs. Not called for the initial render.

The snapshot is only present if getSnapshotBeforeUpdate is present and returns non-null.

**Parameters:**

| Name | Type |
| ------ | ------ |
| prevProps | `Readonly`<[ILoginProps](../interfaces/_index_d_.iloginprops.md)> |
| prevState | `Readonly`<[ILoginState](../interfaces/_index_d_.iloginstate.md)> |
| `Optional` snapshot | [SS]() |

**Returns:** `void`

___
<a id="componentwillmount"></a>

### `<Optional>` componentWillMount

▸ **componentWillMount**(): `void`

*Inherited from DeprecatedLifecycle.componentWillMount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:624*

Called immediately before mounting occurs, and before `Component#render`. Avoid introducing any side-effects or subscriptions in this method.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use componentDidMount or the constructor instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#initializing-state)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Returns:** `void`

___
<a id="componentwillreceiveprops"></a>

### `<Optional>` componentWillReceiveProps

▸ **componentWillReceiveProps**(nextProps: *`Readonly`<[ILoginProps](../interfaces/_index_d_.iloginprops.md)>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.componentWillReceiveProps*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:653*

Called when the component may be receiving new props. React may call this even if props have not changed, so be sure to compare new and existing props if you only want to handle changes.

Calling `Component#setState` generally does not trigger this method.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use static getDerivedStateFromProps instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#updating-state-based-on-props)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[ILoginProps](../interfaces/_index_d_.iloginprops.md)> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="componentwillunmount"></a>

### `<Optional>` componentWillUnmount

▸ **componentWillUnmount**(): `void`

*Inherited from ComponentLifecycle.componentWillUnmount*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:562*

Called immediately before a component is destroyed. Perform any necessary cleanup in this method, such as cancelled network requests, or cleaning up any DOM elements created in `componentDidMount`.

**Returns:** `void`

___
<a id="componentwillupdate"></a>

### `<Optional>` componentWillUpdate

▸ **componentWillUpdate**(nextProps: *`Readonly`<[ILoginProps](../interfaces/_index_d_.iloginprops.md)>*, nextState: *`Readonly`<[ILoginState](../interfaces/_index_d_.iloginstate.md)>*, nextContext: *`any`*): `void`

*Inherited from DeprecatedLifecycle.componentWillUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:683*

Called immediately before rendering when new props or state is received. Not called for the initial render.

Note: You cannot call `Component#setState` here.

Note: the presence of getSnapshotBeforeUpdate or getDerivedStateFromProps prevents this from being invoked.

*__deprecated__*: 16.3, use getSnapshotBeforeUpdate instead; will stop working in React 17

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#reading-dom-properties-before-an-update)

*__see__*: [https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path](https://reactjs.org/blog/2018/03/27/update-on-async-rendering.html#gradual-migration-path)

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[ILoginProps](../interfaces/_index_d_.iloginprops.md)> |
| nextState | `Readonly`<[ILoginState](../interfaces/_index_d_.iloginstate.md)> |
| nextContext | `any` |

**Returns:** `void`

___
<a id="forceupdate"></a>

###  forceUpdate

▸ **forceUpdate**(callback?: *`undefined` \| `function`*): `void`

*Inherited from Component.forceUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:442*

**Parameters:**

| Name | Type |
| ------ | ------ |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="getsnapshotbeforeupdate"></a>

### `<Optional>` getSnapshotBeforeUpdate

▸ **getSnapshotBeforeUpdate**(prevProps: *`Readonly`<[ILoginProps](../interfaces/_index_d_.iloginprops.md)>*, prevState: *`Readonly`<[ILoginState](../interfaces/_index_d_.iloginstate.md)>*): `SS` \| `null`

*Inherited from NewLifecycle.getSnapshotBeforeUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:603*

Runs before React applies the result of `render` to the document, and returns an object to be given to componentDidUpdate. Useful for saving things such as scroll position before `render` causes changes to it.

Note: the presence of getSnapshotBeforeUpdate prevents any of the deprecated lifecycle events from running.

**Parameters:**

| Name | Type |
| ------ | ------ |
| prevProps | `Readonly`<[ILoginProps](../interfaces/_index_d_.iloginprops.md)> |
| prevState | `Readonly`<[ILoginState](../interfaces/_index_d_.iloginstate.md)> |

**Returns:** `SS` \| `null`

___
<a id="isvalid"></a>

###  isValid

▸ **isValid**(): `boolean`

*Defined in index.d.ts:420*

Method to check if the inputs are valid

**Returns:** `boolean`

___
<a id="render"></a>

###  render

▸ **render**(): `TypeComponent`

*Overrides Component.render*

*Defined in index.d.ts:378*

Method that renders the component

**Returns:** `TypeComponent`

___
<a id="setstate"></a>

###  setState

▸ **setState**<`K`>(state: *`function` \| `null` \| `S` \| `object`*, callback?: *`undefined` \| `function`*): `void`

*Inherited from Component.setState*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:437*

**Type parameters:**

#### K :  `keyof ILoginState`
**Parameters:**

| Name | Type |
| ------ | ------ |
| state | `function` \| `null` \| `S` \| `object` |
| `Optional` callback | `undefined` \| `function` |

**Returns:** `void`

___
<a id="shouldcomponentupdate"></a>

### `<Optional>` shouldComponentUpdate

▸ **shouldComponentUpdate**(nextProps: *`Readonly`<[ILoginProps](../interfaces/_index_d_.iloginprops.md)>*, nextState: *`Readonly`<[ILoginState](../interfaces/_index_d_.iloginstate.md)>*, nextContext: *`any`*): `boolean`

*Inherited from ComponentLifecycle.shouldComponentUpdate*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Login/node_modules/@types/react/index.d.ts:557*

Called to determine whether the change in props and state should trigger a re-render.

`Component` always returns true. `PureComponent` implements a shallow comparison on props and state and returns true if any props or states have changed.

If false is returned, `Component#render`, `componentWillUpdate` and `componentDidUpdate` will not be called.

**Parameters:**

| Name | Type |
| ------ | ------ |
| nextProps | `Readonly`<[ILoginProps](../interfaces/_index_d_.iloginprops.md)> |
| nextState | `Readonly`<[ILoginState](../interfaces/_index_d_.iloginstate.md)> |
| nextContext | `any` |

**Returns:** `boolean`

___

