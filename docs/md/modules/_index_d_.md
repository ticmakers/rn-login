[@ticmakers-react-native/login](../README.md) > ["index.d"](../modules/_index_d_.md)

# External module: "index.d"

## Index

### Classes

* [Login](../classes/_index_d_.login.md)

### Interfaces

* [ILoginModelProps](../interfaces/_index_d_.iloginmodelprops.md)
* [ILoginProps](../interfaces/_index_d_.iloginprops.md)
* [ILoginState](../interfaces/_index_d_.iloginstate.md)

### Type aliases

* [TypeLoginAlign](_index_d_.md#typeloginalign)
* [TypeLoginImageSource](_index_d_.md#typeloginimagesource)
* [TypeLoginLang](_index_d_.md#typeloginlang)

---

## Type aliases

<a id="typeloginalign"></a>

###  TypeLoginAlign

**Ƭ TypeLoginAlign**: *"left" \| "center" \| "right"*

*Defined in index.d.ts:28*

Type to define the align available

___
<a id="typeloginimagesource"></a>

###  TypeLoginImageSource

**Ƭ TypeLoginImageSource**: *`ImageSourcePropType`*

*Defined in index.d.ts:18*

Type to define the prop source of the Login component

___
<a id="typeloginlang"></a>

###  TypeLoginLang

**Ƭ TypeLoginLang**: *"es" \| "en" \| "fr" \| "fa"*

*Defined in index.d.ts:23*

Type to define the language available

___

