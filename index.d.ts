import Login, { ILoginProps, ILoginState, TypeLoginImageSource, TypeLoginLang, ILoginModelProps } from './src/index.d'

/**
 * Declaration for Login module
 */
declare module '@ticmakers-react-native/login'

export default Login

export {
  ILoginProps,
  ILoginState,
  TypeLoginImageSource,
  TypeLoginLang,
  ILoginModelProps,
}
