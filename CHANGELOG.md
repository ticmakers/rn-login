# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Released

## [1.1.0] - 2019-10-15

### Added

- Added title component
- Added top container
- Added actions container

### Fixed

- Fixed typings

## [1.0.1] - 2019-09-26

### Fixed

- Fixed dependencies version

## [1.0.0] - 2019-05-15

### Release

### Added

- Added docs api (html, md)

# Unreleased

## [1.0.0-beta.1] - 2019-05-15

### Fixed

- Fix default declaration for Login class

## [1.0.0-beta.0] - 2019-05-14

### Added

- Upload and publish first version

[1.1.0]: https://bitbucket.org/ticmakers/rn-login/src/v1.1.0/
[1.0.1]: https://bitbucket.org/ticmakers/rn-login/src/v1.0.1/
[1.0.0]: https://bitbucket.org/ticmakers/rn-login/src/v1.0.0/
