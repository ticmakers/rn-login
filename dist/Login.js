"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var react_native_1 = require("react-native");
var native_base_1 = require("native-base");
var tinycolor = require("tinycolor2");
var core_1 = require("@ticmakers-react-native/core");
var flexbox_1 = require("@ticmakers-react-native/flexbox");
var image_1 = require("@ticmakers-react-native/image");
var icon_1 = require("@ticmakers-react-native/icon");
var input_1 = require("@ticmakers-react-native/input");
var button_1 = require("@ticmakers-react-native/button");
var styles_1 = require("./styles");
var Login = (function (_super) {
    __extends(Login, _super);
    function Login() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.labels = {
            en: {
                password: 'Password',
                username: 'Username',
            },
            es: {
                password: 'Contraseña',
                username: 'Usuario',
            },
        };
        return _this;
    }
    Login.prototype.render = function () {
        var _a = this._processProps(), containerStyle = _a.containerStyle, shadowBox = _a.shadowBox;
        var containerProps = {
            containerStyle: react_native_1.StyleSheet.flatten([styles_1.default.container, shadowBox && styles_1.default.boxShadow, containerStyle]),
            style: react_native_1.StyleSheet.flatten([styles_1.default.wrapper]),
        };
        return (React.createElement(flexbox_1.Grid, __assign({}, containerProps),
            React.createElement(flexbox_1.Row, { size: 2 }, this.Logo()),
            React.createElement(flexbox_1.Row, { size: 1 }, this.Top()),
            React.createElement(flexbox_1.Row, { size: 3 }, this.Form()),
            React.createElement(flexbox_1.Row, { size: 1 }, this.Actions()),
            React.createElement(flexbox_1.Row, { size: 1 }, this.Footer())));
    };
    Login.prototype.Logo = function () {
        var _a = this._processProps(), LogoComponent = _a.LogoComponent, hideLogo = _a.hideLogo, logoContainerStyle = _a.logoContainerStyle, logoSize = _a.logoSize, logoSource = _a.logoSource;
        var containerProps = {
            style: react_native_1.StyleSheet.flatten([styles_1.default.logoContainer, logoContainerStyle]),
        };
        var imageProps = {
            containerStyle: react_native_1.StyleSheet.flatten([styles_1.default.logoImageContainer]),
            source: logoSource,
            style: react_native_1.StyleSheet.flatten([styles_1.default.logoImage, logoSize && { height: logoSize }]),
        };
        if (!hideLogo) {
            if (core_1.AppHelper.isComponent(LogoComponent)) {
                return (React.createElement(flexbox_1.Col, __assign({}, containerProps, { direction: "row" }), LogoComponent));
            }
            return (React.createElement(flexbox_1.Col, __assign({}, containerProps),
                React.createElement(image_1.default, __assign({}, imageProps))));
        }
    };
    Login.prototype.Top = function () {
        var _a = this._processProps(), TopComponent = _a.TopComponent, hideTop = _a.hideTop, topContainerStyle = _a.topContainerStyle, title = _a.title, titleStyle = _a.titleStyle;
        var containerProps = {
            style: react_native_1.StyleSheet.flatten([styles_1.default.topContainer, topContainerStyle]),
        };
        var titleProps = {
            style: react_native_1.StyleSheet.flatten([styles_1.default.topTitle, titleStyle]),
        };
        if (!hideTop) {
            var titleComponent = void 0;
            var topComponent = void 0;
            if (title) {
                if (core_1.AppHelper.isComponent(title) || core_1.AppHelper.isElement(title)) {
                    titleComponent = React.cloneElement(title, titleProps);
                }
                else {
                    titleComponent = React.createElement(native_base_1.Text, __assign({}, titleProps), "title");
                }
            }
            if (TopComponent && (core_1.AppHelper.isComponent(TopComponent) || core_1.AppHelper.isElement(TopComponent))) {
                topComponent = React.cloneElement(TopComponent);
            }
            return (React.createElement(flexbox_1.Col, __assign({}, containerProps),
                titleComponent,
                topComponent));
        }
    };
    Login.prototype.Form = function () {
        var _this = this;
        var _a = this._processProps(), ForgotPasswordComponent = _a.ForgotPasswordComponent, alignForgotPassword = _a.alignForgotPassword, forgotPasswordStyle = _a.forgotPasswordStyle, hideForm = _a.hideForm, hideForgotPassword = _a.hideForgotPassword, hideValidationIcons = _a.hideValidationIcons, lang = _a.lang, passwordProps = _a.passwordProps, usernameProps = _a.usernameProps;
        var _lang = lang || 'en';
        var defaultUsernameLabel = this.labels[_lang] && this.labels[_lang].username || 'Username';
        var defaultPasswordLabel = this.labels[_lang] && this.labels[_lang].password || 'Password';
        var inputUserProps = {
            hideValidationIcons: hideValidationIcons,
            containerStyle: react_native_1.StyleSheet.flatten([styles_1.default.formInputContainer]),
            iconLeft: (usernameProps && usernameProps.icon) || { name: 'account-circle', type: 'material-community' },
            label: (usernameProps && usernameProps.label) || defaultUsernameLabel,
            lang: _lang,
            rules: (usernameProps && usernameProps.rules) || { required: true },
        };
        var inputPassProps = {
            hideValidationIcons: hideValidationIcons,
            containerStyle: react_native_1.StyleSheet.flatten([styles_1.default.formInputContainer]),
            iconLeft: (passwordProps && passwordProps.icon) || { name: 'key', type: 'material-community' },
            label: (passwordProps && passwordProps.label) || defaultPasswordLabel,
            lang: _lang,
            password: true,
            rules: (passwordProps && passwordProps.rules) || { required: true },
        };
        var forgotPasswordProps = {
            center: alignForgotPassword === 'center',
            left: alignForgotPassword === 'left',
            link: true,
            right: alignForgotPassword === 'right',
            style: react_native_1.StyleSheet.flatten([{ margin: 16 }, forgotPasswordStyle]),
            title: _lang === 'es' ? '¿Olvidaste la contraseña?' : 'Forgot password?',
        };
        if (!hideForm) {
            var forgotPassword = React.createElement(button_1.default, __assign({}, forgotPasswordProps));
            if (ForgotPasswordComponent && core_1.AppHelper.isComponent(ForgotPasswordComponent)) {
                forgotPassword = React.cloneElement(ForgotPasswordComponent, forgotPasswordProps);
            }
            return (React.createElement(flexbox_1.Col, { style: [styles_1.default.formContainer] },
                React.createElement(input_1.default, __assign({ ref: function (c) { return _this.userInput = c; } }, inputUserProps)),
                React.createElement(input_1.default, __assign({ ref: function (c) { return _this.passInput = c; } }, inputPassProps)),
                !hideForgotPassword && forgotPassword,
                this.SubmitButton()));
        }
    };
    Login.prototype.Actions = function () {
        var _a = this._processProps(), ActionsComponent = _a.ActionsComponent, hideActions = _a.hideActions, actionsContainerStyle = _a.actionsContainerStyle;
        var containerProps = {
            style: react_native_1.StyleSheet.flatten([styles_1.default.actionsContainer, actionsContainerStyle]),
        };
        if (!hideActions) {
            if (ActionsComponent && (core_1.AppHelper.isComponent(ActionsComponent) || core_1.AppHelper.isElement(ActionsComponent))) {
                return React.cloneElement(ActionsComponent, containerProps);
            }
            return (React.createElement(flexbox_1.Col, __assign({}, containerProps),
                this.FacebookButton(),
                this.GoogleButton()));
        }
    };
    Login.prototype.SubmitButton = function () {
        var _a = this._processProps(), SubmitComponent = _a.SubmitComponent, lang = _a.lang, submitLabel = _a.submitLabel, submitStyle = _a.submitStyle;
        var _lang = lang || 'en';
        var buttonProps = {
            block: true,
            onPress: this._onSubmit.bind(this),
            style: react_native_1.StyleSheet.flatten([styles_1.default.btn, styles_1.default.formSubmit, submitStyle]),
            title: submitLabel ? submitLabel : _lang === 'es' ? 'Ingresar' : 'Enter',
        };
        if (core_1.AppHelper.isComponent(SubmitComponent)) {
            return React.cloneElement(SubmitComponent, { onPress: this._onSubmit.bind(this) });
        }
        return React.createElement(button_1.default, __assign({}, buttonProps));
    };
    Login.prototype.FacebookButton = function () {
        var _this = this;
        var _a = this._processProps(), FacebookComponent = _a.FacebookComponent, facebook = _a.facebook, facebookIcon = _a.facebookIcon, facebookLabel = _a.facebookLabel, facebookStyle = _a.facebookStyle, lang = _a.lang;
        var _lang = lang || 'en';
        var buttonProps = {
            block: true,
            onPress: function () { return _this._onSubmitFacebook(); },
            style: react_native_1.StyleSheet.flatten([styles_1.default.btn, facebookStyle]),
        };
        var iconName = react_native_1.Platform.select({ android: 'facebook', ios: 'logo-facebook' });
        var iconType = react_native_1.Platform.select({ android: 'font-awesome', ios: 'ionicon' });
        var IconComponent = React.createElement(icon_1.default, { name: iconName, type: iconType, color: "white" });
        if (core_1.AppHelper.isComponent(facebookIcon)) {
            IconComponent = React.cloneElement(facebookIcon);
        }
        else if (facebookIcon && Object.keys(facebookIcon).length > 0) {
            IconComponent = React.createElement(icon_1.default, __assign({}, facebookIcon));
        }
        if (facebook) {
            if (core_1.AppHelper.isComponent(FacebookComponent)) {
                return React.cloneElement(FacebookComponent);
            }
            return (React.createElement(button_1.default, __assign({}, buttonProps),
                IconComponent,
                React.createElement(native_base_1.Text, null, facebookLabel ? facebookLabel : _lang === 'es' ? 'Ingresar con Facebook' : 'Enter with Facebook')));
        }
    };
    Login.prototype.GoogleButton = function () {
        var _this = this;
        var _a = this._processProps(), GoogleComponent = _a.GoogleComponent, google = _a.google, googleIcon = _a.googleIcon, googleLabel = _a.googleLabel, googleStyle = _a.googleStyle, lang = _a.lang;
        var _lang = lang || 'en';
        var buttonProps = {
            block: true,
            light: true,
            onPress: function () { return _this._onSubmitGoogle(); },
            style: react_native_1.StyleSheet.flatten([styles_1.default.btn, googleStyle]),
        };
        var iconName = react_native_1.Platform.select({ android: 'google', ios: 'logo-google' });
        var iconType = react_native_1.Platform.select({ android: 'font-awesome', ios: 'ionicon' });
        var IconComponent = React.createElement(icon_1.default, { name: iconName, type: iconType, color: "white" });
        if (core_1.AppHelper.isComponent(googleIcon)) {
            IconComponent = React.cloneElement(googleIcon);
        }
        else if (googleIcon && Object.keys(googleIcon).length > 0) {
            IconComponent = React.createElement(icon_1.default, __assign({}, googleIcon));
        }
        if (google) {
            if (core_1.AppHelper.isComponent(GoogleComponent)) {
                return React.cloneElement(GoogleComponent);
            }
            return (React.createElement(button_1.default, __assign({}, buttonProps),
                IconComponent,
                React.createElement(native_base_1.Text, null, googleLabel ? googleLabel : _lang === 'es' ? 'Ingresar con Google' : 'Enter with Google')));
        }
    };
    Login.prototype.Footer = function () {
        var _a = this._processProps(), FooterComponent = _a.FooterComponent, hideFooter = _a.hideFooter, lang = _a.lang;
        var _lang = lang || 'en';
        if (!hideFooter) {
            if (core_1.AppHelper.isComponent(FooterComponent)) {
                return (React.createElement(flexbox_1.Col, { style: [styles_1.default.footerContainer], direction: "row" }, FooterComponent));
            }
            return (React.createElement(flexbox_1.Col, { style: [styles_1.default.footerContainer], direction: "row" },
                React.createElement(native_base_1.Text, null, _lang === 'es' ? '¿No tienes una cuenta?' : 'Don\'t have an account?'),
                React.createElement(button_1.default, { link: true, center: true, title: _lang === 'es' ? 'Regístrate' : 'Sign Up' })));
        }
    };
    Login.prototype.isValid = function () {
        var userValue = this.userInput && this.userInput.state.value;
        var userIsValid = this.userInput && this.userInput.isValid();
        var passValue = this.passInput && this.passInput.state.value;
        var passIsValid = this.passInput && this.passInput.isValid();
        return (!!userValue && !!userIsValid) && (!!passValue && !!passIsValid);
    };
    Login.prototype._onSubmit = function () {
        var onSubmit = this._processProps().onSubmit;
        if (onSubmit) {
            return onSubmit();
        }
    };
    Login.prototype._onSubmitFacebook = function () {
        var onFacebook = this._processProps().onFacebook;
        if (onFacebook) {
            return onFacebook();
        }
    };
    Login.prototype._onSubmitGoogle = function () {
        var onGoogle = this._processProps().onGoogle;
        if (onGoogle) {
            return onGoogle();
        }
    };
    Login.prototype._isLight = function (backgroundColor) {
        var isLight = true;
        if (backgroundColor) {
            isLight = tinycolor(backgroundColor).getBrightness() > 200;
        }
        return isLight;
    };
    Login.prototype._processProps = function () {
        var _a = this.props, ActionsComponent = _a.ActionsComponent, FacebookComponent = _a.FacebookComponent, ForgotPasswordComponent = _a.ForgotPasswordComponent, FooterComponent = _a.FooterComponent, GoogleComponent = _a.GoogleComponent, LogoComponent = _a.LogoComponent, SubmitComponent = _a.SubmitComponent, TopComponent = _a.TopComponent, actionsContainerStyle = _a.actionsContainerStyle, alignForgotPassword = _a.alignForgotPassword, containerStyle = _a.containerStyle, facebook = _a.facebook, facebookIcon = _a.facebookIcon, facebookLabel = _a.facebookLabel, facebookStyle = _a.facebookStyle, forgotPasswordStyle = _a.forgotPasswordStyle, google = _a.google, googleIcon = _a.googleIcon, googleLabel = _a.googleLabel, googleStyle = _a.googleStyle, hideFooter = _a.hideFooter, hideForgotPassword = _a.hideForgotPassword, hideActions = _a.hideActions, hideForm = _a.hideForm, hideLogo = _a.hideLogo, hideValidationIcons = _a.hideValidationIcons, hideTop = _a.hideTop, lang = _a.lang, logoContainerStyle = _a.logoContainerStyle, logoSize = _a.logoSize, logoSource = _a.logoSource, onFacebook = _a.onFacebook, onForgotPassword = _a.onForgotPassword, onGoogle = _a.onGoogle, onRegister = _a.onRegister, onSubmit = _a.onSubmit, passwordProps = _a.passwordProps, shadowBox = _a.shadowBox, submitLabel = _a.submitLabel, submitStyle = _a.submitStyle, title = _a.title, titleStyle = _a.titleStyle, topContainerStyle = _a.topContainerStyle, usernameProps = _a.usernameProps;
        var props = {
            ActionsComponent: (typeof ActionsComponent !== 'undefined' ? ActionsComponent : undefined),
            FacebookComponent: (typeof FacebookComponent !== 'undefined' ? FacebookComponent : undefined),
            FooterComponent: (typeof FooterComponent !== 'undefined' ? FooterComponent : undefined),
            ForgotPasswordComponent: (typeof ForgotPasswordComponent !== 'undefined' ? ForgotPasswordComponent : undefined),
            GoogleComponent: (typeof GoogleComponent !== 'undefined' ? GoogleComponent : undefined),
            LogoComponent: (typeof LogoComponent !== 'undefined' ? LogoComponent : undefined),
            SubmitComponent: (typeof SubmitComponent !== 'undefined' ? SubmitComponent : undefined),
            TopComponent: (typeof TopComponent !== 'undefined' ? TopComponent : undefined),
            actionsContainerStyle: (typeof actionsContainerStyle !== 'undefined' ? actionsContainerStyle : undefined),
            alignForgotPassword: (typeof alignForgotPassword !== 'undefined' ? alignForgotPassword : 'right'),
            containerStyle: (typeof containerStyle !== 'undefined' ? containerStyle : undefined),
            facebook: (typeof facebook !== 'undefined' ? facebook : false),
            facebookIcon: (typeof facebookIcon !== 'undefined' ? facebookIcon : undefined),
            facebookLabel: (typeof facebookLabel !== 'undefined' ? facebookLabel : undefined),
            facebookStyle: (typeof facebookStyle !== 'undefined' ? facebookStyle : undefined),
            forgotPasswordStyle: (typeof forgotPasswordStyle !== 'undefined' ? forgotPasswordStyle : undefined),
            google: (typeof google !== 'undefined' ? google : false),
            googleIcon: (typeof googleIcon !== 'undefined' ? googleIcon : undefined),
            googleLabel: (typeof googleLabel !== 'undefined' ? googleLabel : undefined),
            googleStyle: (typeof googleStyle !== 'undefined' ? googleStyle : undefined),
            hideActions: (typeof hideActions !== 'undefined' ? hideActions : false),
            hideFooter: (typeof hideFooter !== 'undefined' ? hideFooter : false),
            hideForgotPassword: (typeof hideForgotPassword !== 'undefined' ? hideForgotPassword : false),
            hideForm: (typeof hideForm !== 'undefined' ? hideForm : false),
            hideLogo: (typeof hideLogo !== 'undefined' ? hideLogo : false),
            hideTop: (typeof hideTop !== 'undefined' ? hideTop : false),
            hideValidationIcons: (typeof hideValidationIcons !== 'undefined' ? hideValidationIcons : false),
            lang: (typeof lang !== 'undefined' ? lang : 'en'),
            logoContainerStyle: (typeof logoContainerStyle !== 'undefined' ? logoContainerStyle : undefined),
            logoSize: (typeof logoSize !== 'undefined' ? logoSize : 80),
            logoSource: (typeof logoSource !== 'undefined' ? logoSource : require('./../assets/icon.png')),
            onFacebook: (typeof onFacebook !== 'undefined' ? onFacebook : undefined),
            onForgotPassword: (typeof onForgotPassword !== 'undefined' ? onForgotPassword : undefined),
            onGoogle: (typeof onGoogle !== 'undefined' ? onGoogle : undefined),
            onRegister: (typeof onRegister !== 'undefined' ? onRegister : undefined),
            onSubmit: (typeof onSubmit !== 'undefined' ? onSubmit : undefined),
            passwordProps: (typeof passwordProps !== 'undefined' ? passwordProps : undefined),
            shadowBox: (typeof shadowBox !== 'undefined' ? shadowBox : false),
            submitLabel: (typeof submitLabel !== 'undefined' ? submitLabel : undefined),
            submitStyle: (typeof submitStyle !== 'undefined' ? submitStyle : undefined),
            title: (typeof title !== 'undefined' ? title : undefined),
            titleStyle: (typeof titleStyle !== 'undefined' ? titleStyle : undefined),
            topContainerStyle: (typeof topContainerStyle !== 'undefined' ? topContainerStyle : undefined),
            usernameProps: (typeof usernameProps !== 'undefined' ? usernameProps : undefined),
        };
        return props;
    };
    return Login;
}(React.Component));
exports.default = Login;
//# sourceMappingURL=Login.js.map