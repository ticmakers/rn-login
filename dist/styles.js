"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_native_1 = require("react-native");
var logoSize = 90;
exports.styles = react_native_1.StyleSheet.create({
    btn: {
        marginBottom: 8,
    },
    wrapper: {
        paddingTop: 50,
    },
    container: {},
    logoContainer: {
        justifyContent: 'center',
    },
    logoImageContainer: {},
    logoImage: {
        height: logoSize,
        resizeMode: 'contain',
    },
    topContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        paddingVertical: 16,
    },
    topTitle: {
        alignItems: 'center',
        alignSelf: 'center',
        fontSize: 20,
    },
    formContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        minHeight: 220,
    },
    formInputContainer: {
        width: '90%',
    },
    formSubmit: {
        marginTop: 16,
    },
    actionsContainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        paddingVertical: 8,
    },
    footerContainer: {
        alignItems: 'center',
        justifyContent: 'center',
        paddingVertical: 8,
    },
    boxShadow: {
        elevation: 5,
        shadowColor: '#FAE057',
        shadowOffset: {
            height: 2,
            width: 0,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
    },
});
exports.default = exports.styles;
//# sourceMappingURL=styles.js.map