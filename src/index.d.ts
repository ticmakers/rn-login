import * as React from 'react'
import {
  ImageSourcePropType,
  RecursiveArray,
  RegisteredStyle,
  StyleProp,
  ViewProps,
  ViewStyle,
} from 'react-native'

import { TypeComponent, TypeStyle } from '@ticmakers-react-native/core'
import Input, { IInputRules } from '@ticmakers-react-native/input'
import { IIconProps } from '@ticmakers-react-native/icon'

/**
 * Type to define the prop source of the Login component
 */
export type TypeLoginImageSource = ImageSourcePropType

/**
 * Type to define the language available
 */
export type TypeLoginLang = 'es' | 'en' | 'fr' | 'fa'

/**
 * Type to define the align available
 */
export type TypeLoginAlign = 'left' | 'center' | 'right'

/**
 * Interface to define the props of the username and password
 * @interface ILoginModelProps
 */
export interface ILoginModelProps {
  /**
   * Label to show in the input
   * @type {string}
   */
  label?: string

  /**
   * Define the rules validation for the input
   * @type {IInputRules}
   */
  rules?: IInputRules

  /**
   * Set a IIconProps or a React-Native component to show a icon for the input
   * @type {(IIconProps | TypeComponent)}
   */
  icon?: IIconProps | TypeComponent

  /**
   * Set a default value to the input
   * @type {*}
   */
  value?: any
}

/**
 * Interface to define the props of the Login component
 * @interface ILoginProps
 */
export interface ILoginProps extends ViewProps {
  /**
   * A React-Native component to replace the Actions component
   * @type {TypeComponent}
   */
  ActionsComponent?: TypeComponent

  /**
   * A React-Native component to replace the button Facebook
   * @type {TypeComponent}
   */
  FacebookComponent?: TypeComponent

  /**
   * A React-Native component to replace the forgot password
   * @type {TypeComponent}
   */
  ForgotPasswordComponent?: TypeComponent

  /**
   * A React-Native component to replace the footer component
   * @type {TypeComponent}
   */
  FooterComponent?: TypeComponent

  /**
   * A React-Native component to replace the button Google
   * @type {TypeComponent}
   */
  GoogleComponent?: TypeComponent

  /**
   * A React-Native component to replace the logo
   * @type {TypeComponent}
   */
  LogoComponent?: TypeComponent

  /**
   * A React-Native component to replace the button submit
   * @type {TypeComponent}
   */
  SubmitComponent?: TypeComponent

  /**
   * A React-Native component to replace the Top component
   * @type {TypeComponent}
   */
  TopComponent?: TypeComponent

  /**
   * Apply a custom style to the actions container
   * @type {TypeStyle}
   */
  actionsContainerStyle?: TypeStyle

  /**
   * Define the align to show the forgot password
   * @type {TypeLoginAlign}
   * @memberof ILoginProps
   */
  alignForgotPassword?: TypeLoginAlign

  /**
   * Apply a custom style to the container
   * @type {TypeStyle}
   */
  containerStyle?: TypeStyle

  /**
   * Set true to show the button Facebook
   * @type {boolean}
   */
  facebook?: boolean

  /**
   * A React-Native component or an object IconProps to replace the icon to Facebook
   * @type {(TypeComponent | IIconProps)}
   */
  facebookIcon?: TypeComponent | IIconProps

  /**
   * A string to define the label to the button Facebook
   * @type {string}
   */
  facebookLabel?: string

  /**
   * Apply a custom style to the button Facebook
   * @type {TypeStyle}
   */
  facebookStyle?: TypeStyle

  /**
   * Apply a custom style to ForgotPassword component
   * @type {TypeStyle}
   */
  forgotPasswordStyle?: TypeStyle

  /**
   * Set true to show the button Google
   * @type {boolean}
   */
  google?: boolean

  /**
   * A React-Native component or an object IconProps to replace the icon to Google
   * @type {(TypeComponent | IIconProps)}
   */
  googleIcon?: TypeComponent | IIconProps

  /**
   * A String to define the label to the button Google
   * @type {string}
   */
  googleLabel?: string

  /**
   * Apply a custom style to the button Google
   * @type {TypeStyle}
   */
  googleStyle?: TypeStyle

  /**
   * Set true to hide the actions component
   * @type {boolean}
   * @default false
   */
  hideActions?: boolean

  /**
   * Set true to hide the footer component
   * @type {boolean}
   * @default false
   */
  hideFooter?: boolean

  /**
   * Set true to hide the forgot password button
   * @type {boolean}
   * @default false
   */
  hideForgotPassword?: boolean

  /**
   * Set true to hide the form component
   * @type {boolean}
   * @default false
   */
  hideForm?: boolean

  /**
   * Set true to hide the logo component
   * @type {boolean}
   * @default false
   */
  hideLogo?: boolean

  /**
   * Set true to hide the icon validations
   * @type {boolean}
   * @default false
   */
  hideValidationIcons?: boolean

  /**
   * Set true to hide the top component
   * @type {boolean}
   * @default false
   */
  hideTop?: boolean

  /**
   * Define the language to show the messages
   * @type {TypeLoginLang}
   * @default en
   */
  lang?: TypeLoginLang

  /**
   * Apply a custom style to the logo container
   * @type {TypeStyle}
   */
  logoContainerStyle?: TypeStyle

  /**
   * A number to define the size of the logo
   * @type {number}
   */
  logoSize?: number

  /**
   * Source to define the image of the logo
   * @type {TypeLoginImageSource}
   */
  logoSource?: TypeLoginImageSource

  /**
   * Method that fire when the button Facebook is pressed
   */
  onFacebook?: () => any

  /**
   * Method that fire when the button Google is pressed
   */
  onGoogle?: () => any

  /**
   * Method that fire when the button submit is pressed
   */
  onSubmit?: () => any

  /**
   * Method that fire when the button register is pressed
   */
  onRegister?: () => any

  /**
   * Method that fire when the button forgot password is pressed
   */
  onForgotPassword?: () => any

  /**
   * Define the props for the input password
   * @type {ILoginModelProps}
   */
  passwordProps?: ILoginModelProps

  /**
   * Set true to apply a shadow box to the container
   * @type {boolean}
   * @default false
   */
  shadowBox?: boolean

  /**
   * A String to define the label to the button submit
   * @type {string}
   */
  submitLabel?: string

  /**
   * Apply a custom style to the button submit
   * @type {TypeStyle}
   */
  submitStyle?: TypeStyle

  /**
   * Apply a custom style to the top container
   * @type {TypeStyle}
   */
  topContainerStyle?: TypeStyle

  /**
   * Show a title before the form container
   * @type {string}
   */
  title?: TypeComponent | string

  /**
   * Apply a custom style to the top title
   * @type {TypeStyle}
   */
  titleStyle?: TypeStyle

  /**
   * Define the props for the input username
   * @type {ILoginModelProps}
   */
  usernameProps?: ILoginModelProps
}

/**
 * Interface to define the state of the Login component
 * @interface ILoginState
 */
export interface ILoginState {
}

/**
 * Class to define the Login component
 * @class Login
 * @extends {React.Component<ILoginProps, ILoginState>}
 */
declare class Login extends React.Component<ILoginProps, ILoginState> {
  /**
   * Reference of username input
   * @type {(Input | undefined)}
   */
  public userInput: Input | undefined

  /**
   * Reference of password input
   * @type {(Input | undefined)}
   */
  public passInput: Input | undefined

  /**
   * Default locale labels
   */
  public labels: {
    en: {
      password: string
      username: string
    },
    es: {
      password: string
      username: string
    }
  }

  /**
   * Method that renders the component
   * @returns {TypeComponent}
   */
  public render(): TypeComponent

  /**
   * Method that renders the Logo component
   * @returns {TypeComponent}
   */
  public Logo(): TypeComponent

  /**
   * Method that renders the Form component
   * @returns {TypeComponent}
   */
  public Form(): TypeComponent

  /**
   * Method that renders the Submit button component
   * @returns {TypeComponent}
   */
  public SubmitButton(): TypeComponent

  /**
   * Method that renders the Facebook button component
   * @returns {TypeComponent}
   */
  public FacebookButton(): TypeComponent

  /**
   * Method that renders the Google button component
   * @returns {TypeComponent}
   */
  public GoogleButton(): TypeComponent

  /**
   * Method that renders the Footer component
   * @returns {TypeComponent}
   */
  public Footer(): TypeComponent

  /**
   * Method to check if the inputs are valid
   * @returns {boolean}
   */
  public isValid(): boolean

  /**
   * Method that fire when the submit button is pressed
   * @private
   * @returns {void}
   */
  private _onSubmit(): void

  /**
   * Method that fire when the button facebook is pressed
   * @private
   * @returns {void}
   */
  private _onSubmitFacebook(): void

  /**
   * Method that fire when the button google is pressed
   * @private
   * @returns {void}
   */
  private _onSubmitGoogle(): void

  /**
   * Method to valid if a background color is light
   * @private
   * @param {string} backgroundColor    A string color rgb,rgba,hex,etc...
   * @returns {boolean}
   */
  private _isLight(backgroundColor: string): boolean

  /**
   * Method to process the props
   * @private
   * @returns {ILoginState}
   */
  private _processProps(): ILoginState
}

export default Login
