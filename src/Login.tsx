import * as React from 'react'
import { StyleSheet, Platform } from 'react-native'
import { Text } from 'native-base'
import * as tinycolor from 'tinycolor2'

import { AppHelper, TypeComponent } from '@ticmakers-react-native/core'
import { Col, Grid, Row } from '@ticmakers-react-native/flexbox'
import Image from '@ticmakers-react-native/image'
import Icon from '@ticmakers-react-native/icon'
import Input, { IInputProps } from '@ticmakers-react-native/input'
import Button from '@ticmakers-react-native/button'

import { ILoginProps, ILoginState, TypeLoginImageSource } from './index.d'
import styles from './styles'

/**
 * Class to define the Login component
 * @class Login
 * @extends {React.Component<ILoginProps, ILoginState>}
 */
export default class Login extends React.Component<ILoginProps, ILoginState> {
  /**
   * Reference of username input
   * @type {(Input | undefined)}
   */
  public userInput: Input | undefined

  /**
   * Reference of password input
   * @type {(Input | undefined)}
   */
  public passInput: Input | undefined

  /**
   * Default locale labels
   */
  public labels = {
    en: {
      password: 'Password',
      username: 'Username',
    },
    es: {
      password: 'Contraseña',
      username: 'Usuario',
    },
  }

  /**
   * Method that renders the component
   * @returns {TypeComponent}
   */
  public render(): TypeComponent {
    const { containerStyle, shadowBox } = this._processProps()

    const containerProps = {
      containerStyle: StyleSheet.flatten([styles.container,  shadowBox && styles.boxShadow, containerStyle]),
      style: StyleSheet.flatten([styles.wrapper]),
    }

    return (
      <Grid { ...containerProps }>
        {/** Logo Container */}
        <Row size={2}>
          { this.Logo() }
        </Row>

        {/** Top Container */}
        <Row size={1}>
          { this.Top() }
        </Row>

        {/** Form Container */}
        <Row size={3}>
          { this.Form() }
        </Row>

        {/** Actions Container */}
        <Row size={1}>
          { this.Actions() }
        </Row>

        {/** Footer Container */}
        <Row size={1}>
          { this.Footer() }
        </Row>
      </Grid>
    )
  }

  /**
   * Method that renders the Logo component
   * @returns {TypeComponent}
   */
  public Logo(): TypeComponent {
    const { LogoComponent, hideLogo, logoContainerStyle, logoSize, logoSource } = this._processProps()
    const containerProps = {
      style: StyleSheet.flatten([styles.logoContainer, logoContainerStyle]),
    }

    const imageProps = {
      containerStyle: StyleSheet.flatten([styles.logoImageContainer]),
      source: logoSource as TypeLoginImageSource,
      style: StyleSheet.flatten([styles.logoImage, logoSize && { height: logoSize } as any]),
    }

    if (!hideLogo) {
      if (AppHelper.isComponent(LogoComponent)) {
        return (
          <Col { ...containerProps } direction="row">
            { LogoComponent }
          </Col>
        )
      }

      return (
        <Col { ...containerProps }>
          <Image { ...imageProps } />
        </Col>
      )
    }
  }

  /**
   * Method that renders the Top component
   * @returns {TypeComponent}
   */
  public Top(): TypeComponent {
    const { TopComponent, hideTop, topContainerStyle, title, titleStyle } = this._processProps()

    const containerProps = {
      style: StyleSheet.flatten([styles.topContainer, topContainerStyle]),
    }

    const titleProps = {
      style: StyleSheet.flatten([styles.topTitle, titleStyle]),
    }

    if (!hideTop) {
      let titleComponent
      let topComponent

      if (title) {
        if (AppHelper.isComponent(title) || AppHelper.isElement(title)) {
          titleComponent = React.cloneElement(title as any, titleProps)
        } else {
          titleComponent = <Text { ...titleProps }>title</Text>
        }
      }

      if (TopComponent && (AppHelper.isComponent(TopComponent) || AppHelper.isElement(TopComponent))) {
        topComponent = React.cloneElement(TopComponent as any)
      }

      return (
        <Col { ...containerProps }>
          { titleComponent }
          { topComponent }
        </Col>
      )
    }
  }

  /**
   * Method that renders the Form component
   * @returns {TypeComponent}
   */
  public Form(): TypeComponent {
    const { ForgotPasswordComponent, alignForgotPassword, forgotPasswordStyle, hideForm, hideForgotPassword, hideValidationIcons, lang, passwordProps, usernameProps } = this._processProps()
    const _lang = lang || 'en'
    const defaultUsernameLabel = (this.labels as any)[_lang] && (this.labels as any)[_lang].username || 'Username'
    const defaultPasswordLabel = (this.labels as any)[_lang] && (this.labels as any)[_lang].password || 'Password'

    const inputUserProps: IInputProps = {
      hideValidationIcons,
      // tslint:disable-next-line: object-literal-sort-keys
      containerStyle: StyleSheet.flatten([styles.formInputContainer]),
      iconLeft: (usernameProps && usernameProps.icon) || { name: 'account-circle', type: 'material-community' },
      label: (usernameProps && usernameProps.label) || defaultUsernameLabel,
      lang: _lang,
      rules: (usernameProps && usernameProps.rules) || { required: true },
    }

    const inputPassProps: IInputProps = {
      hideValidationIcons,
      // tslint:disable-next-line: object-literal-sort-keys
      containerStyle: StyleSheet.flatten([styles.formInputContainer]),
      iconLeft: (passwordProps && passwordProps.icon) || { name: 'key', type: 'material-community' },
      label: (passwordProps && passwordProps.label) || defaultPasswordLabel,
      lang: _lang,
      password: true,
      rules: (passwordProps && passwordProps.rules) || { required: true },
    }

    const forgotPasswordProps = {
      center: alignForgotPassword === 'center',
      left: alignForgotPassword === 'left',
      link: true,
      right: alignForgotPassword === 'right',
      style: StyleSheet.flatten([{ margin: 16 }, forgotPasswordStyle]),
      title: _lang === 'es' ? '¿Olvidaste la contraseña?' : 'Forgot password?',
    }

    if (!hideForm) {
      let forgotPassword = <Button { ...forgotPasswordProps } />

      if (ForgotPasswordComponent && AppHelper.isComponent(ForgotPasswordComponent)) {
        forgotPassword = React.cloneElement(ForgotPasswordComponent as any, forgotPasswordProps)
      }

      return (
        <Col style={ [styles.formContainer] }>
          <Input
            ref={ (c: Input) => this.userInput = c }
            { ...inputUserProps }
          />

          <Input
            ref={ (c: Input) => this.passInput = c }
            { ...inputPassProps }
          />

          { !hideForgotPassword && forgotPassword }

          { this.SubmitButton() }
        </Col>
      )
    }
  }

  /**
   * Method that renders the Actions component
   * @returns {TypeComponent}
   */
  public Actions(): TypeComponent {
    const { ActionsComponent, hideActions, actionsContainerStyle } = this._processProps()

    const containerProps = {
      style: StyleSheet.flatten([styles.actionsContainer, actionsContainerStyle]),
    }

    if (!hideActions) {
      if (ActionsComponent && (AppHelper.isComponent(ActionsComponent) || AppHelper.isElement(ActionsComponent))) {
        return React.cloneElement(ActionsComponent as any, containerProps)
      }

      return (
        <Col { ...containerProps }>
          { this.FacebookButton() }
          { this.GoogleButton() }
        </Col>
      )
    }
  }

  /**
   * Method that renders the Submit button component
   * @returns {TypeComponent}
   */
  public SubmitButton(): TypeComponent {
    const { SubmitComponent, lang, submitLabel, submitStyle } = this._processProps()
    const _lang = lang || 'en'

    const buttonProps = {
      block: true,
      onPress: this._onSubmit.bind(this),
      style: StyleSheet.flatten([styles.btn, styles.formSubmit, submitStyle]),
      title: submitLabel ? submitLabel : _lang === 'es' ? 'Ingresar' : 'Enter',
    }

    if (AppHelper.isComponent(SubmitComponent)) {
      return React.cloneElement(SubmitComponent as any, { onPress: this._onSubmit.bind(this) })
    }

    return <Button { ...buttonProps } />
  }

  /**
   * Method that renders the Facebook button component
   * @returns {TypeComponent}
   */
  public FacebookButton(): TypeComponent {
    const { FacebookComponent, facebook, facebookIcon, facebookLabel, facebookStyle, lang } = this._processProps()
    const _lang = lang || 'en'
    const buttonProps = {
      block: true,
      onPress: () => this._onSubmitFacebook(),
      style: StyleSheet.flatten([styles.btn, facebookStyle]),
    }

    const iconName = Platform.select({ android: 'facebook', ios: 'logo-facebook' })
    const iconType = Platform.select({ android: 'font-awesome', ios: 'ionicon' })
    let IconComponent = <Icon name={ iconName } type={ iconType as any } color="white" />

    if (AppHelper.isComponent(facebookIcon)) {
      IconComponent = React.cloneElement(facebookIcon as any)
    } else if (facebookIcon && Object.keys(facebookIcon).length > 0) {
      IconComponent = <Icon { ...facebookIcon as any } />
    }

    if (facebook) {
      if (AppHelper.isComponent(FacebookComponent)) {
        return React.cloneElement(FacebookComponent as any)
      }

      return (
        <Button { ...buttonProps }>
          { IconComponent }
          <Text>{ facebookLabel ? facebookLabel : _lang === 'es' ? 'Ingresar con Facebook' : 'Enter with Facebook' }</Text>
        </Button>
      )
    }
  }

  /**
   * Method that renders the Google button component
   * @returns {TypeComponent}
   */
  public GoogleButton(): TypeComponent {
    const { GoogleComponent, google, googleIcon, googleLabel, googleStyle, lang } = this._processProps()
    const _lang = lang || 'en'
    const buttonProps = {
      block: true,
      light: true,
      onPress: () => this._onSubmitGoogle(),
      style: StyleSheet.flatten([styles.btn, googleStyle]),
    }

    const iconName = Platform.select({ android: 'google', ios: 'logo-google' })
    const iconType = Platform.select({ android: 'font-awesome', ios: 'ionicon' })
    let IconComponent = <Icon name={ iconName } type={ iconType as any } color="white" />

    if (AppHelper.isComponent(googleIcon)) {
      IconComponent = React.cloneElement(googleIcon as any)
    } else if (googleIcon && Object.keys(googleIcon).length > 0) {
      IconComponent = <Icon { ...googleIcon as any } />
    }

    if (google) {
      if (AppHelper.isComponent(GoogleComponent)) {
        return React.cloneElement(GoogleComponent as any)
      }

      return (
        <Button { ...buttonProps }>
          { IconComponent }
          <Text>{ googleLabel ? googleLabel : _lang === 'es' ? 'Ingresar con Google' : 'Enter with Google' }</Text>
        </Button>
      )
    }
  }

  /**
   * Method that renders the Footer component
   * @returns {TypeComponent}
   */
  public Footer(): TypeComponent {
    const { FooterComponent, hideFooter, lang } = this._processProps()
    const _lang = lang || 'en'

    if (!hideFooter) {
      if (AppHelper.isComponent(FooterComponent)) {
        return (
          <Col style={ [styles.footerContainer] } direction="row">
            { FooterComponent }
          </Col>
        )
      }

      return (
        <Col style={ [styles.footerContainer] } direction="row">
          <Text>{ _lang === 'es' ? '¿No tienes una cuenta?' : 'Don\'t have an account?' }</Text>
          <Button link center title={ _lang === 'es' ? 'Regístrate' : 'Sign Up' } />
        </Col>
      )
    }
  }

  /**
   * Method to check if the inputs are valid
   * @returns {boolean}
   */
  public isValid(): boolean {
    const userValue = this.userInput && this.userInput.state.value
    const userIsValid = this.userInput && this.userInput.isValid()
    const passValue = this.passInput && this.passInput.state.value
    const passIsValid = this.passInput && this.passInput.isValid()

    return (!!userValue && !!userIsValid) && (!!passValue && !!passIsValid)
  }

  /**
   * Method that fire when the submit button is pressed
   * @private
   * @returns {void}
   */
  private _onSubmit(): void {
    const { onSubmit } = this._processProps()

    if (onSubmit) {
      return onSubmit()
    }
  }

  /**
   * Method that fire when the button facebook is pressed
   * @private
   * @returns {void}
   */
  private _onSubmitFacebook(): void {
    const { onFacebook } = this._processProps()

    if (onFacebook) {
      return onFacebook()
    }
  }

  /**
   * Method that fire when the button google is pressed
   * @private
   * @returns {void}
   */
  private _onSubmitGoogle(): void {
    const { onGoogle } = this._processProps()

    if (onGoogle) {
      return onGoogle()
    }
  }

  /**
   * Method to valid if a background color is light
   * @private
   * @param {string} backgroundColor    A string color rgb,rgba,hex,etc...
   * @returns {boolean}
   */
  private _isLight(backgroundColor: string): boolean {
    let isLight = true

    if (backgroundColor) {
      isLight = tinycolor(backgroundColor).getBrightness() > 200
    }

    return isLight
  }

  /**
   * Method to process the props
   * @private
   * @returns {ILoginProps}
   */
  private _processProps(): ILoginProps {
    const { ActionsComponent, FacebookComponent, ForgotPasswordComponent, FooterComponent, GoogleComponent, LogoComponent, SubmitComponent, TopComponent, actionsContainerStyle, alignForgotPassword, containerStyle, facebook, facebookIcon, facebookLabel, facebookStyle, forgotPasswordStyle, google, googleIcon, googleLabel, googleStyle, hideFooter, hideForgotPassword, hideActions, hideForm, hideLogo, hideValidationIcons, hideTop, lang, logoContainerStyle, logoSize, logoSource, onFacebook, onForgotPassword, onGoogle, onRegister, onSubmit, passwordProps, shadowBox, submitLabel, submitStyle, title, titleStyle, topContainerStyle, usernameProps } = this.props

    const props: ILoginProps = {
      ActionsComponent: (typeof ActionsComponent !== 'undefined' ? ActionsComponent : undefined),
      FacebookComponent: (typeof FacebookComponent !== 'undefined' ? FacebookComponent : undefined),
      FooterComponent: (typeof FooterComponent !== 'undefined' ? FooterComponent : undefined),
      ForgotPasswordComponent: (typeof ForgotPasswordComponent !== 'undefined' ? ForgotPasswordComponent : undefined),
      GoogleComponent: (typeof GoogleComponent !== 'undefined' ? GoogleComponent : undefined),
      LogoComponent: (typeof LogoComponent !== 'undefined' ? LogoComponent : undefined),
      SubmitComponent: (typeof SubmitComponent !== 'undefined' ? SubmitComponent : undefined),
      TopComponent: (typeof TopComponent !== 'undefined' ? TopComponent : undefined),
      actionsContainerStyle: (typeof actionsContainerStyle !== 'undefined' ? actionsContainerStyle : undefined),
      alignForgotPassword: (typeof alignForgotPassword !== 'undefined' ? alignForgotPassword : 'right'),
      containerStyle: (typeof containerStyle !== 'undefined' ? containerStyle : undefined),
      facebook: (typeof facebook !== 'undefined' ? facebook : false),
      facebookIcon: (typeof facebookIcon !== 'undefined' ? facebookIcon : undefined),
      facebookLabel: (typeof facebookLabel !== 'undefined' ? facebookLabel : undefined),
      facebookStyle: (typeof facebookStyle !== 'undefined' ? facebookStyle : undefined),
      forgotPasswordStyle: (typeof forgotPasswordStyle !== 'undefined' ? forgotPasswordStyle : undefined),
      google: (typeof google !== 'undefined' ? google : false),
      googleIcon: (typeof googleIcon !== 'undefined' ? googleIcon : undefined),
      googleLabel: (typeof googleLabel !== 'undefined' ? googleLabel : undefined),
      googleStyle: (typeof googleStyle !== 'undefined' ? googleStyle : undefined),
      hideActions: (typeof hideActions !== 'undefined' ? hideActions : false),
      hideFooter: (typeof hideFooter !== 'undefined' ? hideFooter : false),
      hideForgotPassword: (typeof hideForgotPassword !== 'undefined' ? hideForgotPassword : false),
      hideForm: (typeof hideForm !== 'undefined' ? hideForm : false),
      hideLogo: (typeof hideLogo !== 'undefined' ? hideLogo : false),
      hideTop: (typeof hideTop !== 'undefined' ? hideTop : false),
      hideValidationIcons: (typeof hideValidationIcons !== 'undefined' ? hideValidationIcons : false),
      lang: (typeof lang !== 'undefined' ? lang : 'en'),
      logoContainerStyle: (typeof logoContainerStyle !== 'undefined' ? logoContainerStyle : undefined),
      logoSize: (typeof logoSize !== 'undefined' ? logoSize : 80),
      logoSource: (typeof logoSource !== 'undefined' ? logoSource : require('./../assets/icon.png')),
      onFacebook: (typeof onFacebook !== 'undefined' ? onFacebook : undefined),
      onForgotPassword: (typeof onForgotPassword !== 'undefined' ? onForgotPassword : undefined),
      onGoogle: (typeof onGoogle !== 'undefined' ? onGoogle : undefined),
      onRegister: (typeof onRegister !== 'undefined' ? onRegister : undefined),
      onSubmit: (typeof onSubmit !== 'undefined' ? onSubmit : undefined),
      passwordProps: (typeof passwordProps !== 'undefined' ? passwordProps : undefined),
      shadowBox: (typeof shadowBox !== 'undefined' ? shadowBox : false),
      submitLabel: (typeof submitLabel !== 'undefined' ? submitLabel : undefined),
      submitStyle: (typeof submitStyle !== 'undefined' ? submitStyle : undefined),
      title: (typeof title !== 'undefined' ? title : undefined),
      titleStyle: (typeof titleStyle !== 'undefined' ? titleStyle : undefined),
      topContainerStyle: (typeof topContainerStyle !== 'undefined' ? topContainerStyle : undefined),
      usernameProps: (typeof usernameProps !== 'undefined' ? usernameProps : undefined),
    }

    return props
  }
}
