# TIC Makers - React Native Login
React native component for login.

Powered by [TIC Makers](https://ticmakers.com)

## Demo

[Login Expo's snack]()

## Install

Install `@ticmakers-react-native/login` package and save into `package.json`:

NPM
```shell
$ npm install @ticmakers-react-native/login --save
```

Yarn
```shell
$ yarn add @ticmakers-react-native/login
```

## How to use?

```javascript
import React from 'react'
import Login from '@ticmakers-react-native/login'

export default class App extends React.Component {

  render() {
    const passProps = {
      icon: { name: passIcon, type: typeIcon, color: '#76808b' },
      rules: { required: true, minLength: 6, maxLength: 65 },
    }
    const  userProps = {
      icon: { name: userIcon, type: typeIcon, color: '#76808b' },
      rules: { required: true, minLength: 6, maxLength: 64 },
    }

    return (
      <Login
        hideForgotPassword
        containerStyle={ styles.loginContainer }
        lang="es"
        logoSource={ require('./../../assets/images/icon.png') }
        logoSize={ 120 }
        passwordProps={ passProps as any }
        ref={ (e: Login) => this.login = e }
        submitLabel="Iniciar sesión"
        submitStyle={ styles.loginSubmit }
        usernameProps={ userProps as any }
        onSubmit={ this.onSubmit.bind(this) }
      />
    )
  }
}
```

## Properties

| Name | Type | Default Value | Definition |
| ---- | ---- | ------------- | ---------- |
| ---- | ---- | ---- | ----

## Todo

- Test on iOS
- Improve and add new features
- Add more styles
- Create tests
- Add new props and components in readme
- Improve README

## Version 1.1.0 ([Changelog])

[Changelog]: https://bitbucket.org/ticmakers/rn-login/src/master/CHANGELOG.md
